const site = 'http://lobinhos.herokuapp.com/wolves'
var botao = document.querySelector('.trueBotao')

botao.addEventListener('click', () => {
    let tudo = {
        nome: document.querySelector('#nome').value,
        especie: document.querySelector('#especie').value,
        idade: document.querySelector('#idade').value,
        link: document.querySelector('#link').value,
        descricao: document.querySelector('#descricao').value,
    }
    let post = {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(tudo),
    }
    fetch(site, post)
        .then((resposta) => resposta.json())
        .then((data) => {
            console.log('sucesso:', data)
        })
        .catch((erro) => {
            console.error('Erro:', erro)
        })
})